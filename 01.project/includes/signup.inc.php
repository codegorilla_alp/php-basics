<?php
// We controleren vervolgens of de gebruiker op de aanmelding heeft geklikt
if (isset($_POST['submit'])) {
    
    // Vervolgens nemen we de databaseverbinding op
    include('dbh.inc.php');
    
    // En we krijgen de gegevens van het formulier
    $first = $_POST['first'];
    $last = $_POST['last'];
    $email = $_POST['email'];
    $uid = $_POST['uid'];
    $pwd = $_POST['pwd'];

    // Controleer of de invulvelden leeg zijn
    if (empty($first) || empty($last) || empty($email) || empty($uid) || empty($pwd)) {
    header('Location: ../index.php?signup=empty');
    exit();
    } else {
        // Controleer of invoertekens geldig zijn
        if (!preg_match('/^[a-zA-Z]*$/', $first) || !preg_match('/^[a-zA-Z]*$/', $last)) {
            header('Location: ../index.php?signup=char');
            exit();
        } else {
            // Controleer of e-mail geldig is
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
              header('Location: ../index.php?signup=email');
              exit();
            } else {
                header('Location: ../index.php?signup=succes');
                exit();
            }
        }
    } 
} else {
  header ('Location: ../index.php');
  exit();
}
?>