<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">
    <title>01. Project - Creating Database and connect with PHP</title>
</head>

<body>
    <div class="container">

        <section>
            <h1>Signup</h1>
            <form class="contact-form" action="includes/signup.inc.php" method="POST">
                <input type="text" name="first" placeholder="First name">
                <br>
                <input type="text" name="last" placeholder="Last name">
                <br>
                <input type="text" name="email" placeholder="E-mail">
                <br>
                <input type="text" name="uid" placeholder="User name">
                <br>
                <input type="password" name="pwd" placeholder="Password">
                <br>
                <button type="submit" name="submit">Sign Up</button>
            </form>
            <?php
                $fullUrl = 'http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]';
                if(isset($_GET['signup']) && $_GET['signup'] == 'empty'){
                    echo "<p class='error'>You did not full in all fields!</p>"; 
                }


                if (strpos($fullUrl, "signup=empty") == true) {
                    echo "<p class='error'>You did not full in all fields!</p>";
                    exit();
                }
                elseif (strpos($fullUrl, 'signup=char') == true) {
                    echo '<p class="error">You used invalid charachters!</p>';
                    exit();
                }
                elseif (strpos($fullUrl, 'signup=email') == true) {
                    echo '<p class="error">You used an invalid e-mail!</p>';
                    exit();
                }
                elseif (strpos($fullUrl, 'signup=succes') == true) {
                    echo '<p class="succes">You have been signed up!</p>';
                    exit();
                }
            ?>
        </section>
    </div>
</body>

</html>